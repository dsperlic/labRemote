# Tools

**listSupportPS**: tool to list all power supply classes registered and tested  power supply models

**powersupply**: tool to control a generic power supply. For details on how to use, see  --help 

**gpibscan**: tool to determine the GPIB address of your instrument for a USB port (optional)

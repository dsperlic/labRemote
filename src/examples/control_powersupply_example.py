from labRemote import ps, ec

from argparse import ArgumentParser
from pathlib import Path
import sys
import logging
logging.basicConfig()
logger = logging.getLogger("control_powersupply_example")

def control_powersupply_example(config_file, channel_name) :

    ##
    ## print the user input
    ##
    logger.info("Settings:")
    logger.info(f" Configuration file : {config_file}")
    logger.info(f" Channel name       : {channel_name}")

    ##
    ## setup the hw
    ##
    equip_config = ec.EquipConf()
    try :
        equip_config.setHardwareConfig(config_file)
    except :
        logger.error("Unable to load hardware configuration")
        sys.exit(1)

    ##
    ## get the requested power supply channel
    ##
    ps_channel = hw.getPowerSupplyChannel(channel_name)
    if ps_channel is None :
        logger.error(f"Unable to get power supply channel \"{channel_name}\"")
        sys.exit(1)

    ##
    ## do some programming control and measurement actions
    ##
    logger.info(f"Program channel \"{channel_name}\"")
    ps_channel.program()

    logger.info(f"Turn on channel \"{channel_name}\"")
    ps_channel.turnOn()

    logger.info(f"Channel \"{channel_name}\" measurements:")
    logger.info(f"  Voltage: {ps_channel.measureVoltage()}")
    logger.info(f"  Current: {ps_channel.measureCurrent()}")

    ##
    ## Agilent-specific stuff
    ##
    agilent = ps_channel.getPowerSupply()
    if not isinstance(agilent, ps.AgilentPs) :
        logger.warning("The power supply is not an Agilent power supply")
    else :
        logger.info("Switch off beeping for Agilent powersupply")
        agilent.beepOff()

    logger.info(f"Turning of channel \"{channel_name}\"")
    ps_channel.turnOff()

    logger.info("Done!")

if __name__ == "__main__":

    parser = ArgumentParser(description = "Powersupply control example")
    parser.add_argument("config-file", type = str,
        help = "labRemote JSON configuration file for the power supply device"
    )
    parser.add_argument("channel-name", type = str,
        help = "Powersupply channel to control"
    )
    args = parser.parse_args()

    config = Path(args.config_file)
    if not config.exists() or not config.is_file() :
        logger.error(f"Provided configuration file \"{args.config_file}\" could not be found")
        sys.exit(1)
    control_powersupply_example(args.config_file, args.channel_name)

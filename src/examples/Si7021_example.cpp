#include <memory>
#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>

#include <sys/stat.h>
#include <dirent.h>

#include "Si7021.h"

#include "Logger.h"
#include "I2CDevComuino.h"
#include "I2CDevCom.h"


int main(int argc, char* argv[]) {
    std::shared_ptr<TextSerialCom> TC(new TextSerialCom("/dev/ttyACM2", B9600));
    TC->setTermination("\r\n");
    TC->init();
    std::shared_ptr<I2CCom> i2c(new I2CDevComuino(0x40, TC));

    Si7021 tempsensor(i2c);
    tempsensor.init();

  // Loop and measure
    while(true)
       {
	 // Perform the measurement
	 std::time_t time = std::time(nullptr);
	 
	 tempsensor.read();
	 
	 logger (logINFO)  << time << "  " << tempsensor.humidity() << " " << tempsensor.temperature();
       } 
 return 0;
 }


#include "TextSerialCom.h"
#include "NTCSensor.h"
#include "Logger.h"
#include "ADCDevComuino.h"
#include <iostream>
#include <getopt.h>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <cstdint>
#include <string>
#include <memory>

// Voltage powering the NTC
float voltage=5.0;
// The pin on the Arduino connected to the NTC
uint8_t pin=0;
// The address of the device port connected to the Arduino
std::string port=""; 
// Resistance of resistor in voltage divider
float Rvdiv=10000.0;

// Information specific to the NTC
// Part number: 103KT1608
// See https://www.datasheets360.com/pdf/6686957959228959166
// ---------------------------------------------------------
// Reference temperature in Kelvin
float Tref=298.15;
// Resistance at reference temperature in ohms
float Rref=10000.0;
// B value for NTC in Kelvin
float Bntc=3435;

// TDKNTCG103JF103FTDS Steinhart-Hart coefficients
float A = 0.8676453371787721e-3;
float B = 2.541035850140508e-4;
float C = 1.868520310774293e-7;

void usage(char *argv[])
{
  std::cerr << "" << std::endl;
  std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
  std::cerr << "List of options:" << std::endl; 
  std::cerr << " -p, --pin P         Set the Arduino pin number connected to the NTC (default: " << pin << ")" << std::endl;  
  std::cerr << " -r, --resistance R  Set the resistance in the voltage divider (default: " << Rvdiv << ")" << std::endl;
  std::cerr << " -a, --port ADDR   Set the address of the port connected to the Arduino" << std::endl;  
  std::cerr << " -d, --debug         Enable more verbose printout, use multiple for increased debug level"  << std::endl;
  std::cerr << " -h, --help          List the commands and options available"  << std::endl;
  std::cerr << "" << std::endl;
}


int main(int argc, char* argv[]) 
{ 
  //Parse command-line  
  if (argc < 1)
  {
    usage(argv);
    return 1;
   }
  
  int c;
  while (true) {
    int option_index = 0;
    static struct option long_options[] = {
      {"pin", required_argument, 0,  'p' },
      {"resistance",  required_argument, 0,  'r' },
      {"port",  required_argument, 0,  'a' },
      {"debug",    no_argument      , 0,  'd' },
      {"help",     no_argument      , 0,  'h' },
      {0,          0,                 0,  0 }
    };
                                                                                
    c = getopt_long(argc, argv, "p:r:a:dh", long_options, &option_index);
    if (c == -1)  break;
    switch (c) {
      case 'p':
        try
        {
	    pin = (uint8_t)std::stoi(optarg);
        }
        catch(const std::invalid_argument& e)
        {
	    std::cerr << "Pin must be a number. "<< optarg << "supplied. Aborting." << std::endl;
      	    std::cerr << std::endl;
            usage(argv);
	    return 1;
        }
	break;
      case 'a':
	port = optarg;
	break;
      case 'r':
	try
	{
            Rvdiv = std::stoi(optarg);
	}
	catch(const std::invalid_argument& e)
	{
	    std::cerr << "Resistance must be a number. "<< optarg << "supplied. Aborting." << std::endl;
      	    std::cerr << std::endl;
            usage(argv);
	    return 1;
        }
	break;
      case 'd':
	logIt::incrDebug();
	break;
      case 'h':
	usage(argv);
	return 1;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(port=="") {
      std::cerr << "No device port specified for the Arduino. Aborting." <<std::endl;
      usage(argv);
      return 1;
  }

  logger(logDEBUG) << "Device port: " << port;
  logger(logDEBUG) << "Pin: " << (int)pin;

  std::shared_ptr<TextSerialCom> com = std::make_shared<TextSerialCom>(port,B9600);
  com->setTermination("\r\n");
  com->setTimeout(5);
  com->init();
  std::this_thread::sleep_for(std::chrono::seconds(1));
  
  // create Arduino as ADC device
  std::shared_ptr<ADCDevice> dev(new ADCDevComuino(voltage,com));

  NTCSensor sensor(pin, dev, false, Tref, Rref, Bntc, false, Rvdiv, voltage); // read voltage drop over Rdiv, using beta formula for temperature conversion
  //NTCSensor sensor(pin, dev, true, A, B, C, true, Rvdiv, voltage); // read voltage drop over NTC, using Steinhart-Hart for temperature conversion
  sensor.init();

  while (true)
    {
      // Temperature in Celsius
      try {
        sensor.read();
      } catch(const std::runtime_error& e) {
          logger(logDEBUG) << e.what();
          std::this_thread::sleep_for(std::chrono::seconds(1));
          continue;
      }
      float temp=sensor.temperature();
      logger(logINFO) << "Temperature: " << temp << " C";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }

  return 0;
}

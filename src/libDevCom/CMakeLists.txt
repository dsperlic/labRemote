# Commented files are from amacv2_tester version of the library. They still need to be
# ported to labRemote.
add_library(DevCom SHARED)

# These device libraries depend on Linux and will be included
# once we have confirmed (below) that we are on a Linux host
set(LINUX_SPECIFIC_SOURCES SPIDevCom.cpp I2CDevCom.cpp)

target_sources(DevCom
  PRIVATE
  DeviceComRegistry.cpp
  DeviceCom.cpp
  UIOCom.cpp

  I2CCom.cpp
  I2CDevComuino.cpp
  PCA9548ACom.cpp

  SPICom.cpp

  ComIOException.cpp
  NotSupportedException.cpp
  ChecksumException.cpp
  OutOfRangeException.cpp

  CalibratedDevice.cpp

  ClimateSensor.cpp
  #BME280.cpp
  HIH6130.cpp
  HIH4000.cpp
  HTS221.cpp
  SHT85.cpp
  Si7021.cpp  
  NTCSensor.cpp
  PtSensor.cpp

  DACDevice.cpp
  DAC5571.cpp
  DAC5574.cpp
  MCP4801.cpp
  AD56X9.cpp

  ADCDevice.cpp
  AD799X.cpp
  MAX11619.cpp
  MCP3425.cpp
  LTC2451.cpp
  MCP3428.cpp
  ADCDevComuino.cpp

  IOExpander.cpp
  MCP23008.cpp
  MCP23017.cpp

  MuxDevice.cpp
  PGA11x.cpp

  Potentiometer.cpp
  AD5245.cpp

  AMAC.cpp

  DeviceCalibration.cpp
  DummyCalibration.cpp
  LinearCalibration.cpp
  FileCalibration.cpp  
)
target_include_directories(DevCom PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(DevCom PUBLIC Com Utils)

# If we are on macOS, ignore those devices that require the Linux-specific drivers
if("${CMAKE_SYSTEM_NAME}" STREQUAL "Darwin")
    message(STATUS "[libDevCom] macOS system detected, ignoring the following Linux specific devices:")
    foreach(device_file ${LINUX_SPECIFIC_SOURCES})
        get_filename_component(device ${device_file} NAME_WE)
        message(STATUS "  > libDevCom/${device}")
    endforeach()
    set(LINUX_SPECIFIC_SOURCES "")
endif()
target_sources(DevCom
    PRIVATE
    ${LINUX_SPECIFIC_SOURCES}
)

# Optional dependencies
if ( ${ENABLE_FTDI} )
  target_sources(DevCom
    PRIVATE
    MPSSEChip.cpp
    FT232H.cpp
    I2CFTDICom.cpp
    )

  target_include_directories (DevCom PRIVATE ${LIBFTDI1_INCLUDE_DIR} ${LIBMPSSE_INCLUDE_DIR} )
  target_link_libraries (DevCom PRIVATE ${LIBFTDI1_LIBRARIES} ${LIBMPSSE_LIBRARIES} )
else()
  message(STATUS "Skipping FTDICom due to missing libftdi1 or libmpsse")
  message(STATUS " libftdi1 = ${LIBFTDI1_FOUND}")
  message(STATUS " libmpsse = ${LIBMPSSE_FOUND}")
endif()

set_target_properties(DevCom PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS DevCom)

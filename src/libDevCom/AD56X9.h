#ifndef AD56X9_H
#define AD56X9_H

#include <stdint.h>

#include <memory>
#include <map>

#include "I2CCom.h"
#include "DACDevice.h"

struct AD56X9ModelInfo
{
  int32_t MaxValue;
};

class AD56X9 : public DACDevice
{
public:
  enum Model {AD5629, AD5669};

  AD56X9(double reference, Model model, std::shared_ptr<I2CCom> com);
  virtual ~AD56X9();

  virtual void setCount(int32_t counts);
  virtual void setCount(uint8_t ch, int32_t counts);
  virtual void setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& data);

  virtual int32_t readCount();
  virtual int32_t readCount(uint8_t ch);
  virtual void    readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data);

private:
  // Model information
  static const std::map<Model, AD56X9ModelInfo> ModelInfo;
  
  // Properties of device
  Model m_model;
  
  std::shared_ptr<I2CCom> m_com;
};

#endif // AD56X9_H

# Configuration 

The hardware configuration is stored in JSON file.
An example of input JSON file is provided in the configs folder.
Only the following libraries support configuration via json file:
* libPS

The main top-fields of the JSON configuration are:
- version: to ensure compatibility of configuration file with labRemote software version;
- options: version-specific general options (e.g. autoconfiguration of some hardware parameters, if supported);
- devices: list of the hardware devices that are available, and their communication protocol
- channels: list of logical channels available

There is a problem in src/tools, listSupportPS, which lists all the power supply classes that are registered as well as all the tested power supply models.

### Devices section
List of devices that are available, for which the configuration set name, model and communication protocol.
An example of hardware configuration follows just for illustration.
Each device must have a unique name and can have the following properties (required ones marked with *).

  - `hw-type`: type of hardware device; the supported ones are:
     - `PS`: power-supply
  - `hw-model`: model for the particular hardware (name of the dedicated class in labPS)
  - `communication`: details of communication, can be hardware-specific, but must include:
     - `protocol`: communication protocol implementation deriving from `ICom` (for example, "TextSerialCom")

Custom fields that depend on specific hardware can then be present and used as needed. Check the `setConfiguration` documentation for the implementation of the communication and power supply classes.

### Channels section 
List of logical channels. Assigns names to easily map physical channels to logical ones.
Each channel must have a unique name, and can have the following properties (required ones marked witth *).

  - `hw-type`: type of hardware device; the supported ones are:
     - `PS`: power-supply
  - `device`: name of the physical device the channel refers to, as lised in the **devices** section
  - `channel`: channel number in the physics device **device**
  - `program`:  optional list of voltage/current values and voltage/current limis used to program the channel. They are set only if the `program()` function is called. See description of program function in `PowerSupplyChannel` class
  - `datasink`: type of data sink used to take and record measurements

The following examples (stored in `src/examples`) demonstrate how to use different aspects of configuration files:
  - `control_powersupply_example.cpp`: example on how to control a power supply
  - `datasink_example.cpp`: example on how to use data sinks



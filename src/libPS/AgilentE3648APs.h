#ifndef AGILENTE3648APS_H
#define AGILENTE3648APS_H

#include <string>

#include "AgilentPs.h"

/**
 * Implementation for the [Agilent E364xA Dual Channel Output DC Power Supplies](https://www.mouser.com/new/keysight/keysight-e364xa-dc-power-supplies-eu/#TextLinks-5).
 * The dual-channel Agilent power supplies appear to support the same programming
 * model as the single-chanenl E364xA power supplies, but this has not been checked.
 */
class AgilentE3648APs : public AgilentPs
{
    public:
        AgilentE3648APs(const std::string& name);
        ~AgilentE3648APs() = default;
};

#endif // AGILENTE3648APS_H
